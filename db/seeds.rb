
############### CRIAÇÃO DE USUARIOS #######################

puts "Criando usuários"
puts "Administrator"

usuario_administrator = User.new(email: "administrator@email.com", password:"12345678", kind:"administrator")
usuario_administrator.save(validate: false)

puts "Client"
usuario_client1 = User.create!(email:"client1@email.com", password:"12345678", kind:"client")
usuario_client2 = User.create!(email:"client2@email.com", password:"12345678", kind:"client")
#client = Client.create!(name:"José de freitas", cpf:"00011133344", user:usuario_client)
#client.save

puts "Psychologist"
usuario_psychologist1 = User.create!(email:"psychologist1@email.com", password:"12345678", kind:"psychologist")
usuario_psychologist2 = User.create!(email:"psychologist2@email.com", password:"12345678", kind:"psychologist")
#psychologist = Psychologist.create!(name:"Maria santos", crp:"111111", user:usuario_psychologist)
#psychologist.save

puts "Company"
usuario_company1 = User.create!(email:"company1@email.com", password:"12345678", kind:"company")
usuario_company2 = User.create!(email:"company2@email.com", password:"12345678", kind:"company")
#company = Company.create!(name:"José de freitas", agent:"Manoel da silva", cnpj:"00011133344", user:usuario_company)
#company.save
