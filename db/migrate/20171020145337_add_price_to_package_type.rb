class AddPriceToPackageType < ActiveRecord::Migration[5.1]
  def change
    add_column :package_types, :price, :integer, default: 0
  end
end
