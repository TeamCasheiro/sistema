class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.text :description
      t.string :name
      t.string :address
      t.string :telephone
      t.string :email

      t.timestamps
    end
  end
end
