class CreateInterviews < ActiveRecord::Migration[5.1]
  def change
    create_table :interviews do |t|
      t.string :video
      t.string :description
      t.string :score
      t.datetime :start_time
      t.datetime :end_time
      t.integer :status, default: 0
      t.references :package, foreign_key: true, null: false
      t.references :psychologist, foreign_key: true
      t.references :client, foreign_key: true

      t.timestamps
    end
  end
end
