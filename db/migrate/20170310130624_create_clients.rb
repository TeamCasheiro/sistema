class CreateClients < ActiveRecord::Migration[5.0]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :cpf, null: false
      t.references :user, foreign_key: true, unique: true

      t.timestamps
    end

  end
end
