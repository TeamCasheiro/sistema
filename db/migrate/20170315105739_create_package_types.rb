class CreatePackageTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :package_types do |t|
      t.string :name
      t.integer :interview_counter, null: false

      t.timestamps
    end
  end
end
