class CreatePackageClients < ActiveRecord::Migration[5.0]
  def change
    create_table :package_clients do |t|
      t.references :client, foreign_key: true, null: false
      t.references :package, foreign_key: true, null: false

      t.timestamps
    end
  end
end
