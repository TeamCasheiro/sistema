class CreatePackagePsychologists < ActiveRecord::Migration[5.0]
  def change
    create_table :package_psychologists do |t|
      t.references :psychologist, foreign_key: true, null: false
      t.references :package, foreign_key: true, null: false

      t.timestamps
    end
  end
end
