class CreatePackages < ActiveRecord::Migration[5.0]
  def change
    create_table :packages do |t|
      t.references :package_type, foreign_key: true, null: false
      t.references :company, foreign_key: true, null: false
      t.string :description

      t.timestamps
    end
  end
end
