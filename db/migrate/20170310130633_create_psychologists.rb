class CreatePsychologists < ActiveRecord::Migration[5.0]
  def change
    create_table :psychologists do |t|
      t.string :name
      t.string :crp
      t.time :interview_start_period, null: false
      t.time :interview_end_period, null: false
      t.integer :interview_period, default: 30
      t.references :user, foreign_key: true, unique: true, null: false

      t.timestamps
    end
  end
end
