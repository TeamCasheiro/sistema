class AddPaymentToPackage < ActiveRecord::Migration[5.1]
  def change
    add_column :packages, :payment, :integer, default: 0
  end
end
