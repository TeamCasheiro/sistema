class CreatePackagePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :package_payments do |t|
      t.string :code
      t.datetime :date
      t.references :package, foreign_key: true
      t.integer :transaction_type
      t.integer :transaction_status

      t.timestamps
    end
  end
end
