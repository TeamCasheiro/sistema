class AddFigureToContacts < ActiveRecord::Migration[5.0]
  def change
    add_column :contacts, :figure, :string
  end
end
