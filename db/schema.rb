# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171020204738) do

  create_table "clients", force: :cascade do |t|
    t.string "name"
    t.string "cpf", null: false
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_clients_on_user_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.string "agent"
    t.string "cnpj"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_companies_on_user_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "figure"
  end

  create_table "interviews", force: :cascade do |t|
    t.string "video"
    t.string "description"
    t.string "score"
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer "status", default: 0
    t.integer "package_id"
    t.integer "psychologist_id"
    t.integer "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_interviews_on_client_id"
    t.index ["package_id"], name: "index_interviews_on_package_id"
    t.index ["psychologist_id"], name: "index_interviews_on_psychologist_id"
  end

  create_table "messages", force: :cascade do |t|
    t.text "description"
    t.string "name"
    t.string "address"
    t.string "telephone"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "package_clients", force: :cascade do |t|
    t.integer "client_id", null: false
    t.integer "package_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_package_clients_on_client_id"
    t.index ["package_id"], name: "index_package_clients_on_package_id"
  end

  create_table "package_payments", force: :cascade do |t|
    t.string "code"
    t.datetime "date"
    t.integer "package_id"
    t.integer "transaction_type"
    t.integer "transaction_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["package_id"], name: "index_package_payments_on_package_id"
  end

  create_table "package_psychologists", force: :cascade do |t|
    t.integer "psychologist_id", null: false
    t.integer "package_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["package_id"], name: "index_package_psychologists_on_package_id"
    t.index ["psychologist_id"], name: "index_package_psychologists_on_psychologist_id"
  end

  create_table "package_types", force: :cascade do |t|
    t.string "name"
    t.integer "interview_counter", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "price", default: 0
  end

  create_table "packages", force: :cascade do |t|
    t.integer "package_type_id", null: false
    t.integer "company_id", null: false
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "payment", default: 0
    t.index ["company_id"], name: "index_packages_on_company_id"
    t.index ["package_type_id"], name: "index_packages_on_package_type_id"
  end

  create_table "psychologists", force: :cascade do |t|
    t.string "name"
    t.string "crp"
    t.time "interview_start_period", null: false
    t.time "interview_end_period", null: false
    t.integer "interview_period", default: 30
    t.integer "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_psychologists_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "time_zone", default: "Brasilia"
    t.integer "kind"
    t.string "curriculum"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
