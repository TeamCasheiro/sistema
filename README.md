# README

You need to set environment variables referent to PagSeguro payment system
* PAGSEGURO_TOKEN: token from your account.
* PAGSEGURO_EMAIL: email from your account.
* PAGSEGURO_ENV: production || sandbox.
* HOST: host where the site run. E.g. https\:\/\/example.com
