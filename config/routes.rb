Rails.application.routes.draw do
  resources :clients
  resources :companies
  resources :contacts
  resources :messages
  resources :psychologists
  resources :users
  # resources :package_clients
  # resources :package_psychologists
  resources :package_types
  resources :packages do
    member do
      get 'checkout'
    end
  end

  # TODO: fix constraints to accept only requests from pagseguro
  # pagseguro, at least in sandbox, uses different ip's per request, not the specified below
  # ws.pagseguro.uol.com.br || sandbox.pagseguro.uol.com.br
  # constraints(host: 'ws.pagseguro.uol.com.br') do
  post 'package_payments' => 'package_payments#create', as: 'payment_notification'
  # end

  resources :interviews do
    collection do
      get 'calendar'
      get 'schedule'
      patch 'schedule_update/:id' => 'interviews#schedule_update', as: 'schedule_update'
    end
  end

  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }, path: 'u'

  get 'panel(/index)' => 'panels#index', as: 'panel_index'

  root to: 'home#index'
end
