FactoryGirl.define do
  factory :contact
  factory :message
  factory :package_cpf
  factory :package_psychologist
  factory :package
  factory :interview

  factory :package_type do
    interview_counter { rand(1..100) }
  end

  factory :user do
    sequence(:id)         { |n| n }
    sequence(:email)      { |n| "person#{n}@example.com" }
    password              'foobar'
    password_confirmation 'foobar'

    trait :client do
      sequence(:name) { |n| "client #{n}"}
      kind :client
      after(:create) do |user, _evaluator|
        association :client, factory: :client, user: user
      end
    end

    trait :company do
      sequence(:name) { |n| "company #{n}"}
      kind :company
      after(:create) do |user, _evaluator|
        association :company, factory: :company, user: user
      end
    end

    trait :psychologist do
      sequence(:name) { |n| "psychologist #{n}"}
      kind :psychologist
      after(:create) do |user, _evaluator|
        association :psychologist, factory: :psychologist, user: user
      end
    end

    trait :administrator do
      kind :administrator
    end
  end

  require 'cpf_cnpj'
  factory :client do
    sequence(:name) { |n| "client #{n}"}
    cpf { CPF.generate }

    transient do
      association :user, factory: :user, kind: :client
    end

    after(:create) do |client, evaluator|
      client.user = create(:user, kind: :client)
      client.save
    end
  end

  factory :company do
    sequence(:name) { |n| "company #{n}"}

    transient do
      association :user, factory: :user, kind: :company
    end

    after(:create) do |_self, evaluator|
      association :user, evaluator.user
    end
  end

  factory :psychologist do
    sequence(:name) { |n| "psychologist #{n}"}

    transient do
      association :user, factory: :user, kind: :psychologist
    end

    after(:create) do |_self, evaluator|
      association :user, evaluator.user
    end
  end
end
