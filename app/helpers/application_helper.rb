module ApplicationHelper
  def format_date(date)
    return nil if date.nil?
    date.strftime('%d/%m/%Y')
  end

  def format_time(time)
    return nil if time.nil?
    time.strftime('%H:%M')
  end

  def format_output(value)
    return '' if value.nil?
    if [ActiveSupport::TimeWithZone, DateTime, Date, Time].member?(value.class)
      l(value)
    else
      value
    end
  end

  def form_control_render(f, field, permitted_attributes = [], &block)
    return if field.nil? || permitted_attributes.empty?
    return unless permitted_attributes.include?(field)

    input = yield
    # asda
    # return render(partial: 'layouts/forms/custom_field', locals: { field: field, permitted_attributes: permitted_attributes, input: input }) unless input.nil?

    render 'layouts/forms/field', locals: { f: f, field: field, permitted_attributes: permitted_attributes }
  end
end
