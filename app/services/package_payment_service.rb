class PackagePaymentService
  include Rails.application.routes.url_helpers

  def initialize(package)
    @package = package

    raise 'Payment needs a package!' if @package.nil? || !package.is_a?(Package)
  end

  # return reponse to be redirected
  def generate_payment
    payment = PagSeguro::PaymentRequest.new

    payment.reference = "package-#{@package.id}"
    payment.notification_url = "#{ENV['HOST']}#{payment_notification_path}"
    payment.redirect_url = "#{ENV['HOST']}#{package_path(@package.id)}"

    payment.items << {
      id: @package.id,
      description: "Pacote '#{@package.package_type}'#{' - ' unless @package.description.nil?}#{@package.description}",
      amount: @package.package_type.price,
      weight: 0
    }

    response = payment.register
    @package.update(payment: 'waiting') unless response.errors.any?
    response
  end
end
