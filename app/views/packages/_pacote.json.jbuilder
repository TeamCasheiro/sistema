json.extract! package, :id, :package_type_id, :created_at, :updated_at
json.url package_url(package, format: :json)
