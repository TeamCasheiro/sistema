json.extract! message, :id, :description, :name, :address, :telephone, :created_at, :updated_at
json.url message_url(message, format: :json)
