json.extract! company, :id, :name, :agent, :cnpj, :created_at, :updated_at
json.url company_url(company, format: :json)
