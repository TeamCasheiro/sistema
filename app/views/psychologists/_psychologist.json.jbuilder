json.extract! psychologist, :id, :name, :crp, :created_at, :updated_at
json.url psychologist_url(psychologist, format: :json)
