json.extract! package_psychologist, :id, :psychologist_id, :created_at, :updated_at
json.url package_psychologist_url(package_psychologist, format: :json)
