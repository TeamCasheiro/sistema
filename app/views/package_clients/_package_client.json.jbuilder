json.extract! package_client, :id, :client_id, :created_at, :updated_at
json.url package_client_url(package_client, format: :json)
