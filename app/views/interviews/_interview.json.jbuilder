json.extract! interview, :id, :video, :description, :score, :start_time, :end_time, :package_id, :psychologist_id, :client_id, :created_at, :updated_at
json.url interview_url(interview, format: :json)
