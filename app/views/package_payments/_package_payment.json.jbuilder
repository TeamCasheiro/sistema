json.extract! package_payment, :id, :code, :date, :package_id, :transaction_type, :transaction_status, :created_at, :updated_at
json.url package_payment_url(package_payment, format: :json)
