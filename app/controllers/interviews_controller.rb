class InterviewsController < ApplicationController
  before_action :set_interview, only: [:show, :edit, :update, :destroy, :schedule_update]

  # GET /interviews
  # GET /interviews.json
  def index
    @q = policy_scope(Interview).includes(:client, :package, :psychologist).ransack(params[:q])
    @interviews = @q.result(distinct: true)
  end

  # GET /interviews/1
  # GET /interviews/1.json
  def show
    @permitted_attributes = policy(@interview).permitted_attributes_for_show
  end

  # GET /interviews/new
  def new
    @interview = Interview.new
    authorize @interview
  end

  # GET /interviews/1/edit
  def edit
    @permitted_attributes = policy(@interview).permitted_attributes_for_edit
    @date = params[:date]
    @scheduled_interviews = @interview.package.interviews.where('id <> ? AND start_time IS NOT NULL', @interview.id)
  end

  # POST /interviews
  # POST /interviews.json
  def create
    @interview = Interview.new(interview_params)
    authorize @interview

    respond_to do |format|
      if @interview.save
        format.html { redirect_to @interview, notice: 'Interview was successfully created.' }
        format.json { render :show, status: :created, location: @interview }
      else
        format.html { render :new }
        format.json { render json: @interview.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /interviews/1
  # PATCH/PUT /interviews/1.json
  def update
    respond_to do |format|
      if @interview.update(interview_params)
        format.html { redirect_to @interview, notice: 'Interview was successfully updated.' }
        format.json { render :show, status: :ok, location: @interview }
      else
        @permitted_attributes = policy(@interview).permitted_attributes_for_edit
        format.html { render :edit }
        format.json { render json: @interview.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /interviews/1
  # DELETE /interviews/1.json
  def destroy
    @interview.destroy
    respond_to do |format|
      format.html { redirect_to interviews_url, notice: 'Interview was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /interviews/calendar
  def calendar
    @interviews = policy_scope(Interview).includes(:client, :package, :psychologist)
    @missing = set_missing_interviews
  end

  # GET /interviews/schedule
  def schedule
    @interviews = Interview.where(client_id: current_user.client.id, start_time: nil)
    authorize @interviews
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_interview
      @interview = Interview.find(params[:id])
      authorize @interview
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def interview_params
      ps = params.require(:interview).permit(policy(@interview).permitted_attributes)
      ps.merge(start_end_time_keys)
    end

    # transform interview datetimes from form to schema
    def start_end_time_keys
      return if params['interview']['date'].blank?

      start_time = date_time_from_params('start_time')
      end_time = date_time_from_params('end_time') || start_time + @interview.psychologist.interview_period.minutes

      { start_time: start_time, end_time: end_time }
    end

    def date_time_from_params(time)
      return nil if params['interview'][time].nil?
      DateTime.strptime("#{params['interview']['date']} #{params['interview'][time]} #{Time.now.formatted_offset(false)}", '%Y/%m/%d %H:%M %z')
    end

    def set_missing_interviews
      missing = case current_user.kind
                when 'psychologist'
                  Interview.where(psychologist_id: current_user.psychologist.id, end_time: nil)
                when 'company'
                  Interview.joins(:package).where('packages.company_id = ? AND end_time IS NULL', current_user.company.id)
                when 'client'
                  package_ids = current_user.client.packages.pluck(:id).uniq
                  Interview.where(package: package_ids, client_id: current_user.client.id, end_time: nil)
      end

      missing.nil? ? [] : missing.group(:package).count
    end
end
