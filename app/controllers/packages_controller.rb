class PackagesController < ApplicationController
  before_action :set_package, only: [:show, :edit, :update, :destroy, :checkout]

  # GET /packages
  # GET /packages.json
  def index
    @packages = policy_scope(Package).includes(:package_type)
  end

  # GET /packages/1
  # GET /packages/1.json
  def show
  end

  # GET /packages/new
  def new
    @package = Package.new
    @psychologist = Psychologist.first
  end

  # GET /packages/1/edit
  def edit
  end

  # POST /packages
  # POST /packages.json
  def create
    @package = Package.new(package_params)
    @package.company = current_user.company if current_user.company?
    authorize @package

    respond_to do |format|
      if @package.save
        @package.reload
        @package.create_associated_interviews
        @package.reload
        create_associated_clients(@package)
        format.html { redirect_to @package, notice: 'Package was successfully created.' }
        format.json { render :show, status: :created, location: @package }
      else
        format.html { render :new }
        format.json { render json: @package.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /packages/1
  # PATCH/PUT /packages/1.json
  def update
    respond_to do |format|
      if @package.update(package_params)
        create_associated_clients(@package)
        format.html { redirect_to @package, notice: 'Package was successfully updated.' }
        format.json { render :show, status: :ok, location: @package }
      else
        format.html { render :edit }
        format.json { render json: @package.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /packages/1
  # DELETE /packages/1.json
  def destroy
    @package.destroy
    respond_to do |format|
      format.html { redirect_to packages_url, notice: 'Package was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def checkout
    response = PackagePaymentService.new(@package).generate_payment

    if response.errors.any?
      redirect_to packages_url, alert: 'Não foi possível gerar o pagamento.'
    else
      redirect_to response.url
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_package
    @package = Package.find(params[:id])
    authorize @package
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def package_params
    params.require(:package).permit(:package_type_id, :description, :company_id, package_psychologists_attributes: [:id, :psychologist_id, :package_id, :_destroy])
  end

  def cpf_params
    params.require(:cpf).permit(cpf: [])
  end

  def create_associated_clients(package)
    package.package_clients.destroy_all
    cpf_params['cpf'].first(package.package_type.interview_counter).each do |cpf_string|
      client = Client.find_or_create_by(cpf: CPF.new(cpf_string).stripped)
      PackageClient.find_or_create_by(package_id: package.id, client_id: client.id)
    end

    package.reload
    package.fix_associated_interviews
  end
end
