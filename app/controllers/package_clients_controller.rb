class PackageClientsController < ApplicationController
  before_action :set_package_client, only: [:show, :edit, :update, :destroy]

  # GET /package_clients
  # GET /package_clients.json
  def index
    @package_clients = policy_scope(PackageClient)
  end

  # GET /package_clients/1
  # GET /package_clients/1.json
  def show
  end

  # GET /package_clients/new
  def new
    @package_client = PackageClient.new
    authorize @package_client
  end

  # GET /package_clients/1/edit
  def edit
  end

  # POST /package_clients
  # POST /package_clients.json
  def create
    @package_client = PackageClient.new(package_client_params)
    authorize @package_client

    respond_to do |format|
      if @package_client.save
        format.html { redirect_to @package_client, notice: 'PackageClient was successfully created.' }
        format.json { render :show, status: :created, location: @package_client }
      else
        format.html { render :new }
        format.json { render json: @package_client.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /package_clients/1
  # PATCH/PUT /package_clients/1.json
  def update
    respond_to do |format|
      if @package_client.update(package_client_params)
        format.html { redirect_to @package_client, notice: 'PackageClient was successfully updated.' }
        format.json { render :show, status: :ok, location: @package_client }
      else
        format.html { render :edit }
        format.json { render json: @package_client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /package_clients/1
  # DELETE /package_clients/1.json
  def destroy
    @package_client.destroy
    respond_to do |format|
      format.html { redirect_to package_clients_url, notice: 'PackageClient was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_package_client
      @package_client = PackageClient.find(params[:id])
      authorize @package_client
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def package_client_params
      params.require(:package_client).permit(:client_id)
    end
end
