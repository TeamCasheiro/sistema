class PackagePsychologistsController < ApplicationController
  before_action :set_package_psychologist, only: [:show, :edit, :update, :destroy]

  # GET /package_psychologists
  # GET /package_psychologists.json
  def index
    @package_psychologists = policy_scope(PackagePsychologist)
  end

  # GET /package_psychologists/1
  # GET /package_psychologists/1.json
  def show
  end

  # GET /package_psychologists/new
  def new
    @package_psychologist = PackagePsychologist.new
    authorize @package_psychologist
  end

  # GET /package_psychologists/1/edit
  def edit
  end

  # POST /package_psychologists
  # POST /package_psychologists.json
  def create
    @package_psychologist = PackagePsychologist.new(package_psychologist_params)
    authorize @package_psychologists

    respond_to do |format|
      if @package_psychologist.save
        format.html { redirect_to @package_psychologist, notice: 'PackagePsychologist was successfully created.' }
        format.json { render :show, status: :created, location: @package_psychologist }
      else
        format.html { render :new }
        format.json { render json: @package_psychologist.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /package_psychologists/1
  # PATCH/PUT /package_psychologists/1.json
  def update
    respond_to do |format|
      if @package_psychologist.update(package_psychologist_params)
        format.html { redirect_to @package_psychologist, notice: 'PackagePsychologist was successfully updated.' }
        format.json { render :show, status: :ok, location: @package_psychologist }
      else
        format.html { render :edit }
        format.json { render json: @package_psychologist.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /package_psychologists/1
  # DELETE /package_psychologists/1.json
  def destroy
    @package_psychologist.destroy
    respond_to do |format|
      format.html { redirect_to package_psychologists_url, notice: 'PackagePsychologist was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_package_psychologist
      @package_psychologist = PackagePsychologist.find(params[:id])
      authorize @package_psychologist
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def package_psychologist_params
      params.require(:package_psychologist).permit(:psychologist_id)
    end
end
