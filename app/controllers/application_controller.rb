class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  # policies
  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  # after_action :verify_authorized, except: :index
  # after_action :verify_policy_scoped, only: :index

  # set user time_zone
  before_action :set_time_zone, if: :user_signed_in?

  def after_sign_in_path_for(_resource)
    panel_index_path
  end

  # create user kind model
  before_action :verify_user_kind_existence

  private

  # Overwriting the sign_out redirect path method
  def after_sign_out_path_for(_resource_or_scope)
    new_user_session_path
  end

  def user_not_authorized
    flash[:alert] = 'Você não está autorizado a executar essa ação.'
    redirect_to(request.referrer || panel_index_path)
  end

  def set_time_zone
    Time.zone = current_user.time_zone
  end

  # TODO: find a cheap method that don't verify it in every transaction
  def verify_user_kind_existence
    return if !current_user.present? || current_user.kind_model?

    flash[:alert] = 'Você deve primeiro cadastrar alguns dados necessários para continuar.'
    redirect_to new_kind_path[current_user.kind]
  end

  def new_kind_path
    {
      'client' => new_client_path,
      'psychologist' => new_psychologist_path,
      'company' => new_company_path
    }
  end
end
