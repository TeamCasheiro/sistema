class PackagePaymentsController < ApplicationController
  before_action :set_package_payment, only: [:show, :edit, :update, :destroy]
  skip_before_action :authenticate_user!, only: :create
  skip_before_action :verify_authenticity_token, only: :create
  skip_before_action :verify_user_kind_existence, only: :create

  # GET /package_payments
  # GET /package_payments.json
  def index
    @package_payments = PackagePayment.all
  end

  # GET /package_payments/1
  # GET /package_payments/1.json
  def show
  end

  # GET /package_payments/new
  # def new
  #   @package_payment = PackagePayment.new
  # end

  # GET /package_payments/1/edit
  def edit
  end

  # POST /package_payments
  # POST /package_payments.json
  # def create
  #   @package_payment = PackagePayment.new(package_payment_params)
  #
  #   respond_to do |format|
  #     if @package_payment.save
  #       format.html { redirect_to @package_payment, notice: 'Package payment was successfully created.' }
  #       format.json { render :show, status: :created, location: @package_payment }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @package_payment.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end
  def create
    transaction = PagSeguro::Transaction.find_by_notification_code(params[:notificationCode])

    # TODO: put creation in some job (sidekiq maybe?)
    if transaction.errors.empty?
      payment = PackagePayment.find_or_create_by(code: transaction.code)
      payment.package_id = transaction.reference.gsub('package-', '') if payment.package_id.nil?
      payment.transaction_type = PackagePayment.transaction_types.key(transaction.type_id) if payment.transaction_type.nil?
      payment.transaction_status = PackagePayment.transaction_statuses.key(transaction.status.id)
      payment.save
    end

    render status: 200, layout: false
  end

  # PATCH/PUT /package_payments/1
  # PATCH/PUT /package_payments/1.json
  def update
    respond_to do |format|
      if @package_payment.update(package_payment_params)
        format.html { redirect_to @package_payment, notice: 'Package payment was successfully updated.' }
        format.json { render :show, status: :ok, location: @package_payment }
      else
        format.html { render :edit }
        format.json { render json: @package_payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /package_payments/1
  # DELETE /package_payments/1.json
  # def destroy
  #   @package_payment.destroy
  #   respond_to do |format|
  #     format.html { redirect_to package_payments_url, notice: 'Package payment was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_package_payment
      @package_payment = PackagePayment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def package_payment_params
      params.require(:package_payment).permit(:code, :date, :package_id, :transaction_type, :transaction_status)
    end
end
