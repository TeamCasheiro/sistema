# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->
  $('#cpf-tags').on 'itemAddedOnInit', (ev) ->
    $('form').append("<input type=\"hidden\" id=\"cpf-#{ev.item.replace(/[^\w\s]/gi, '')}\" name=\"cpf[cpf][]\" value=\"#{ev.item}\" \/>")

  $('#cpf-tags').on 'beforeItemAdd', (ev) ->
    $('form').append("<input type=\"hidden\" id=\"cpf-#{ev.item.replace(/[^\w\s]/gi, '')}\" name=\"cpf[cpf][]\" value=\"#{ev.item}\" \/>")

  $('#cpf-tags').on 'itemRemoved', (ev) ->
    $("\#cpf-#{ev.item.replace(/[^\w\s]/gi, '')}").remove()

  $('#cpf-tags input').on 'keypress', (e) ->
    if (e.keyCode == 13)
      e.keyCode = 188
      e.preventDefault()
