# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->
  # change end_time input
  $('.start_time').on 'change', (ev) ->
    start_time = $('.start_time')[0]['value'].split(':')
    end_time = $('.end_time')[0]['value'].split(':')
    if end_time == undefined || (start_time[0] > end_time[0]) || ((start_time[0] >= end_time[0]) && (start_time[1] > end_time[1]))
      picker = $('.end_time').pickatime('picker')
      picker.set('select', $('.start_time')[0]['value'], { format: 'HH:i' })
