$(document).ready ->
  $('[data-toggle="tooltip"]').tooltip()

  $datepicker = $('.datepicker').pickadate({
    format: 'dd/mm/yyyy',
    hiddenSuffix: ''
  })

  $timepicker = $('.timepicker').pickatime({
    format: 'HH:i',
    formatLabel: 'HH:i',
    formatSubmit: 'HH:i',
    interval: 30,
    hiddenSuffix: ''
  })
