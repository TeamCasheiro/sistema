class Client < ApplicationRecord
  belongs_to :user
  has_many :package_clients
  has_many :packages, through: :package_clients
  has_many :interviews

  validates :user, presence:true, uniqueness: true, allow_blank: true
  validates :cpf, presence: true, uniqueness: true
  before_save :format_cpf
  validate :valid_cpf

  def to_s
    "#{name} (#{cpf})"
  end

  def format_cpf
    self.cpf = CPF.new(cpf).stripped
  end

  def valid_cpf
    errors.add(:cpf, 'inválido.') unless CPF.valid?(cpf)
  end
end
