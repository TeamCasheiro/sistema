class PackagePsychologist < ApplicationRecord
  belongs_to :psychologist
  belongs_to :package

  validates :package, presence: true
  validates :psychologist, presence: true, uniqueness: { scope: :package }
end
