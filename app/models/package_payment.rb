class PackagePayment < ApplicationRecord
  belongs_to :package

  enum transaction_type: { payment: 1, recurrent: 11 }
  enum transaction_status: { waiting: 1, analysis: 2, paid: 3, disponible: 4,
                            dispute: 5, returned: 6, canceled: 7, debited: 8,
                            temporary_retention: 9 }

  validates :code, presence: true, uniqueness: true
  validates :package, presence: true

  after_save :update_package_payment_status

  private

  def update_package_payment_status
    payment = if ['waiting', 'analysis', 'dispute', 'temporary_retention'].include? transaction_status
                'waiting'
              elsif ['paid', 'disponible'].include? transaction_status
                'ok'
              else
                'blank'
    end
    package.update(payment: payment)
  end
end
