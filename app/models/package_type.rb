class PackageType < ApplicationRecord
	has_many :packages

	validates :interview_counter, presence: true
	validates :price, presence: true

	def to_s
		name
	end
end
