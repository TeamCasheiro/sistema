class Company < ApplicationRecord
  belongs_to :user

  has_many :packages

  validates :user, presence: true, uniqueness: true

  def to_s
    name
  end
end
