class User < ApplicationRecord
  require 'carrierwave/orm/activerecord'

  mount_uploader :curriculum, CurriculumUploader

  has_one :client
  has_one :psychologist
  has_one :company

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  enum kind: { client: 0, company: 1, psychologist: 2, administrator: 3 }

  def kind_model
    case kind
    when 'client'
      client
    when 'company'
      company
    when 'psychologist'
      psychologist
    end
  end

  def kind_model?
    return true if administrator? || !kind_model.blank?
    false
  end
end
