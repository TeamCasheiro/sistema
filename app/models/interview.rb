class Interview < ApplicationRecord
  belongs_to :psychologist
  belongs_to :client
  belongs_to :package
  has_one :company, through: :package

  enum status: { blank: 0, scheduled: 1, missed: 2, complete: 3 }

  before_save :update_status

  validates :package_id, presence: true
  validate :accept_package_psychologists_only
  validate :accept_package_clients_only
  validate :dont_conflict_time
  validate :start_time_before_end_time
  validate :client_schedule_per_package
  validate :schedule_between_psychologist_period

  private

  def update_status
    return if status == 'missed' || status == 'complete'
    return self.status = 'scheduled' unless start_time.nil? || end_time.nil? || client_id.nil? || psychologist_id.nil?
    self.status = 'blank'
  end

  def accept_package_psychologists_only
    errors.add(:psychologist_id, 'A entrevista deve ter um psicólogo cadastrado no pacote.') if !psychologist_id.nil? && PackagePsychologist.where(package_id: package_id, psychologist_id: psychologist_id).empty?
  end

  def accept_package_clients_only
    errors.add(:client_id, 'A entrevista deve ter um cliente cadastrado no pacote.') if !client_id.nil? && PackageClient.where(package_id: package_id, client_id: client_id).empty?
  end

  def dont_conflict_time
    return if start_time.nil? || end_time.nil?

    if !psychologist_id.nil? && !Interview.where('id <> ? AND psychologist_id = ? AND start_time >= ? AND end_time <= ?', id, psychologist_id, start_time, end_time).empty?
      return errors.add(:start_time, 'O horário conflita com outra entrevista do psicólogo(a).')
    end

    if !client_id.nil? && !Interview.where('id <> ? AND client_id = ? AND start_time >= ? AND end_time <= ?', id, client_id, start_time, end_time).empty?
      return errors.add(:start_time, 'O horário conflita com outra entrevista do cliente.')
    end

    true
  end

  def start_time_before_end_time
    return if start_time.nil? || end_time.nil?
    errors.add(:start_time, 'A entrevista deve começar antes de terminar.') if start_time > end_time
  end

  def client_schedule_per_package
    if !client_id.nil? && !Interview.where('id <> ? AND package_id = ? AND client_id = ?', id, package_id, client_id).empty?
      return errors.add(:client, 'Cliente não pode ter mais que uma entrevista por pacote.')
    end
  end

  def schedule_between_psychologist_period
    return if start_time.nil?
    end_time = start_time + psychologist.interview_period.minutes if end_time.nil?

    pstart = psychologist.interview_start_period
    pend = psychologist.interview_end_period
    if (start_time.hour <= pstart.hour && start_time.min < pstart.min) || (end_time.hour >= pend.hour && end_time.min > pend.min)
      return errors.add(:start_time, 'A entrevista deve ocorrer enquanto o psicólogo pode atender.')
    end
  end
end
