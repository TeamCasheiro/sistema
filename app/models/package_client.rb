class PackageClient < ApplicationRecord
  belongs_to :client
  belongs_to :package

  validates :package, presence: true
  validates :client, presence: true, uniqueness: { scope: :package }
end
