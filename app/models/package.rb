class Package < ApplicationRecord
  belongs_to :package_type
  belongs_to :company
  has_many :interviews, dependent: :destroy
  has_many :package_clients, dependent: :destroy
  has_many :clients, through: :package_clients
  has_many :package_psychologists, dependent: :destroy
  has_many :psychologists, through: :package_psychologists
  has_one :package_payment, dependent: :destroy

  enum payment: { blank: 0, waiting: 1, ok: 2 }

  accepts_nested_attributes_for :package_clients, reject_if: :all_blank, allow_destroy: true
  # validates_associated :package_clients
  accepts_nested_attributes_for :package_psychologists, reject_if: :all_blank, allow_destroy: true
  validates_associated :package_psychologists
  validates :company, presence: true
  validates :package_type, presence: true

  after_save :create_associated_interviews

  def to_s
    "#{company} - #{description}"
  end

  # TODO: clean this
  def fix_associated_interviews
    cpf_removed = interviews.pluck(:client_id).compact - package_clients.pluck(:client_id).compact
    cpf_to_include = package_clients.pluck(:client_id).compact - interviews.pluck(:client_id).compact
    free_interviews = interviews.where('client_id IN (?) OR client_id IS NULL', cpf_removed)
    free_interviews.each_with_index do |interview, i|
      interview.client_id = cpf_to_include[i]
      if cpf_to_include[i].nil?
        interview.start_time = nil
        interview.end_time = nil
      end
      interview.save
    end
  end

  def create_associated_interviews
    return if id.nil? || interviews.count == package_type.interview_counter || payment != 'ok'

    Interview.transaction do
      psychologist_id = package_psychologists.count == 1 ? package_psychologists.first.psychologist_id : nil

      (package_type.interview_counter - interviews.count).times do |i|
        # TODO: improve query, use import gem or something
        Interview.create(package_id: id, psychologist_id: psychologist_id, client_id: package_clients[i].try(:client_id))
      end
    end
  end
end
