class Psychologist < ApplicationRecord
	belongs_to :user
	has_many :package_psychologists
	has_many :packages, through: :package_psychologists
	has_many :interviews

	validates :user, presence: true, uniqueness: true
	validates :interview_start_period, presence: true
	validates :interview_end_period, presence: true
	validate :period_start_before_ends

	def to_s
		name
	end

	private

	def period_start_before_ends
		errors.add(:interview_end_period, 'Período de entrevista deve começar antes de terminar.') if interview_start_period.nil? || interview_end_period.nil? || (interview_start_period >= interview_end_period)
	end
end
