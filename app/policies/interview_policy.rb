class InterviewPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      case user.kind
      when 'client'
        scope.where(client: user.client)
      when 'psychologist'
        scope.where(psychologist: user.psychologist)
      when 'company'
        scope.where(package: user.company.packages)
      when 'administrator'
        scope.all
      end
    end
  end

  def permitted_attributes_for_show
    return [] if !user.present? || (!user.administrator? && record.package.payment != 'ok')

    case user.kind
    when 'psychologist' || 'company' || 'administrator'
      [:video, :description, :score, :package, :psychologist, :client, :start_time, :end_time, :status]
    else
      [:video, :package, :psychologist, :start_time, :end_time, :status]
    end
  end

  # TODO: merge with default if possible
  def permitted_attributes_for_edit
    return [] if !user.present? || (!user.administrator? && record.package.payment != 'ok')

    case user.kind
    when 'psychologist'
      [:video, :description, :score, :status]
    when 'company'
      [:psychologist_id, :client_id, :status]
    when 'administrator'
      [:video, :description, :score, :package_id, :psychologist_id, :client_id, :status]
    when 'client'
      [:date, :start_time]
    end
  end

  def permitted_attributes
    return [] if !user.present? || (!user.administrator? && record.package.payment != 'ok')

    case user.kind
    when 'psychologist'
      [:video, :description, :score, :status]
    when 'company'
      [:psychologist_id, :client_id, :status]
    when 'administrator'
      [:video, :description, :score, :package_id, :psychologist_id, :client_id, :status]
    when 'client'
      []
    end
  end

  def index?
    user.present? && !user.client?
  end

  def show?
    authorize_if_own_interview
  end

  def new?
    user.present? && user.administrator?
  end

  def create?
    user.present? && user.administrator?
  end

  def edit?
    authorize_if_own_interview_and_blank
  end

  def update?
    authorize_if_own_interview_and_blank
  end

  def destroy?
    user.present? && user.administrator?
  end

  def calendar?
    user.present?
  end

  def schedule?
    user.present? && user.client?
  end

  private

  def authorize_if_own_interview
    user.present? && (
      user.administrator? ||
      (user.client? && record.client_id == user.client.id) ||
      (user.psychologist? && record.psychologist_id == user.psychologist.id) ||
      (user.company? && record.package.company_id == user.company.id)
    )
  end

  def authorize_if_own_interview_and_blank
    authorize_if_own_interview && (!user.client? || record.status == 'blank')
  end
end
