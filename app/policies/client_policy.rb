class ClientPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.where.not(user_id: nil)
    end
  end

  def index?
    user.administrator?
  end

  def show?
    user.administrator? || (user.client? && record == user.client)
  end

  def new?
    user.administrator? || (user.client? && !user.kind_model?)
  end

  def create?
    user.administrator? || (user.client? && record == user.client)
  end

  def edit?
    user.administrator?
  end

  def update?
    user.administrator?
  end

  def destroy?
    user.administrator?
  end
end
