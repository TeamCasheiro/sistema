class PsychologistPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def index?
    user.administrator?
  end

  def show?
    user.administrator? || (user.psychologist? && record == user.psychologist)
  end

  def new?
    user.administrator? || (user.psychologist? && !user.kind_model?)
  end

  def create?
    user.administrator? || (user.psychologist? && record == user.psychologist)
  end

  def edit?
    user.administrator?
  end

  def update?
    user.administrator?
  end

  def destroy?
    user.administrator?
  end
end
