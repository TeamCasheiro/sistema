class PackagePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      case user.kind
      when 'client'
        scope.includes(:package_clients).where(package_clients: { client: user.client })
      when 'psychologist'
        scope.includes(:package_psychologists).where(package_psychologists: { psychologist: user.psychologist })
      when 'company'
        scope.where(company: user.company)
      when 'administrator'
        scope.all
      end
    end
  end

  def index?
    user.present? && (user.administrator? || user.company?)
  end

  def show?
    user.present? && (user.administrator? || user.company?)
  end

  def new?
    user.present? && (user.administrator? || user.company?)
  end

  def create?
    user.present? && (user.administrator? || user.company?)
  end

  def edit?
    user.present? && (user.administrator? || company_record)
  end

  def update?
    user.present? && (user.administrator? || company_record)
  end

  def destroy?
    user.present? && (user.administrator? || company_record)
  end

  def checkout?
    user.present? && company_record
  end

  private

  def company_record
    user.company? && (user.company.id == record.company_id)
  end
end
