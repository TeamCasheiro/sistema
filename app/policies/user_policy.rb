class UserPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def index?
    user.administrator?
  end

  def show?
    user.administrator? || user.id == record.id
  end

  def create?
    (!user.present? && !record.administrator?) || (user.administrator? || !record.administrator?)
  end

  def update?
    user.administrator? || (user.id == record.id && !record.administrator?)
  end

  def destroy?
    user.administrator? || user.id == record.id
  end
end
