class CompanyPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def index?
    user.administrator?
  end

  def show?
    user.administrator? || (user.company? && record == user.company)
  end

  def new?
    user.administrator? || (user.company? && !user.kind_model?)
  end

  def create?
    user.administrator? || (user.company? && record == user.company)
  end

  def edit?
    user.administrator? || (user.company? && user.company == record)
  end

  def update?
    user.administrator? || (user.company? && user.company == record)
  end

  def destroy?
    user.administrator?
  end
end
